#include "scheduler.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define init_time_task  0
pthread_mutex_t llave;
int op, num_tasks=0;



int main(void)
{
	
do{
	printf("-----Menu-----\n");	
	printf("1.-Crear tarea\n");
	printf("2.-Listar tareas\n");
	printf("3.-Iniciar tareas\n");
	printf("5.- Salir\n");
	printf("Ingresa una opcion a realizar: ");
	scanf("%d", &op);
switch (op)
	{
	case 1:
	{
		printf("Cuantas Tareas quiere agregara a la cola?:");
		scanf("%d", &num_tasks);				
		DatosdeTarea(num_tasks);					
	break;
	}
	case 2:
	{
	for (int k = 0; k < num_tasks; k++)
	{
	if (task[k].tiempo_ejec > 0)
	{
						
		printf("\n");
		printf("tarea[%i]\n", k);
		printf("Tarea ID: %d\n",task[k].id);
		printf("Nombre de la tarea: %s\n",task[k].nom_task);
		printf("Prioridad de la tarea: %d\n",task[k].prioridad);
		printf("Tiempo de ejecucion %d\n",task[k].tiempo_ejec);
	}
	}
	break;
	}
	case 3:
	{
	char flag[20] = "Finalizado";
	for (int i = 0; i < num_tasks; i++)
	{
	if (task[i].tiempo_ejec==0)
	{
		printf("No hay Tareas en la cola de espera\n");
		break;
	}else
	{
		printf("Tareas ordenadas por prioridad\n");
		ordenar(num_tasks);
	for (int i = 0; i < 50; i++)
	{
		delay(3);
	}
		printf("OK\n");
	for (int i = 0; i < 50; i++)
	{
		delay(2);
	}
		system("clear");
		printf("Prioridad Ordenada\n");

	for (int i = 0; i < num_tasks; i++)
	{
		printf("\n");
		printf("Tarea[%i]\n", i);
		printf("Tarea ID: %d\n",task[i].id);
		printf("Nombre de la tarea: %s\n",task[i].nom_task);
		printf("Prioridad de la tarea: %d\n",task[i].prioridad);
		printf("Tiempo de ejecucion %d\n",task[i].tiempo_ejec);
							
	}
	for (int i = 0; i < 50; i++)
	{
		delay(3);
	}
		printf("Iniciando procesos\n");
	for (int i = 0; i < 50; i++)
	{
		delay(3);
	}
		printf("Por favor espere\n");
	for (int i = 0; i < 50; i++)
	{
		delay(3);
	}
		printf("OK\n");
		system("clear");
		pthread_t hilo1;
		pthread_mutex_init(&llave,NULL);
		pthread_create(&hilo1,NULL,Ejecutartareas,NULL);
		pthread_join(hilo1,NULL);
		printf("Tareas Finalizadas: \n");
	for (int i = 0; i < 50; i++)
	{
	delay(3);
	}
	for (int i = 0; i < num_tasks; i++)
	{
		printf("\n");
		printf("tarea[%i]\n", i);
		printf("|tarea ID: %d\n",task[i].id);
		printf("|nombre de la tarea: %s\n",task[i].nom_task);
		printf("|prioridad de la tarea: %d\n",task[i].prioridad);
		printf("|tiempo de ejecucion %d\n",task[i].tiempo_ejec);
			}
		}
	}
	break;
	}
	case 4:
	{			
		printf("OK\n");
		for (int i = 0; i < 50; i++)
		{
			delay(3);
		}
			num_tasks = 0;
			system("clear");	
			break;
		}
		case 5:
		{
			printf("Saliendo");
		for (int i = 0; i < 50; i++)
		{
			delay(5);
				
		}
			system("clear");
			break;
		}
			default :
		{
			printf("La opcion ingresada no es valida\n");
		for (int i = 0; i < 50; i++)
		{
			delay(5);
		}
			system("clear");
			break;
			}
		}
	}while(op != 5);	
	return 0;
}

void *Ejecutartareas(void *args){
	int i,x=1,y=5;
		printf("Espere para poder bloquear\n\n");
		pthread_mutex_lock(&llave);
		printf("key bloqueada\n\n");
		printf("ejecutando\n");
	for (int i = 0; i < 50; i++){
		delay(4);
	}                            	                          
	for(i = 0; i<num_tasks; i++){
		fflush(stdout);
		printf( "\nTarea:[%s", task[i].nom_task);
		printf( "\nId: [%d", task[i].id);
		printf( "\nPrioridad: [%d", task[i].prioridad); 
		do{      
			printf( "\n%d seg", task[i].tiempo_ejec);
			task[i].tiempo_ejec = task[i].tiempo_ejec - 1;
		    for (int i = 0; i < 50; i++){
				delay(3);
			}
		} while (task[i].tiempo_ejec != 0);
	}
	system("clear");
	printf("\nTerminado\n");
	for(int i = 0; i < num_tasks; i++){      
		printf( "\nid: %d", task[i].id);
		printf( "\nTarea: %s", task[i].nom_task);
		printf( "\nPrioridad: %d", task[i].prioridad);
		printf( "\nTiempo de ejecucion: %d \n", task[i].tiempo_ejec);
	} 
	printf("\n");
	pthread_mutex_unlock(&llave);
	printf("*key bloqueada\n");
	return NULL;
} 


void delay(int num_of_seconds)
{
	int miliseconds = 10000 * num_of_seconds;
	clock_t start_time = clock();
	while(clock() < start_time + miliseconds);
}

void DatosdeTarea(int num_tasks)
{
	for (int i = 0; i < num_tasks; i++)
	{
		printf("T A S K [%d]\n", i);
		printf("Ingrese el id de la tarea: ");
		scanf("%i", &task[i].id);
		printf("Ingrese el nombre de la tarea: ");
		scanf("%s", task[i].nom_task);
		printf("Ingrese la prioridad de la tarea: ");
		scanf("%d", &task[i].prioridad);
		printf("Ingrese el tiempo en que deseas ejecutar la tarea: ");
		scanf("%d", &task[i].tiempo_ejec);
	}
}

void ordenar(int num_tasks){
	int temporal, temporal2, temporal3;
	char temporal4[50];

	for (int i = 0;i < num_tasks; i++){
		for (int j = 0; j< num_tasks-1; j++){
			if (task[j].prioridad < task[j+1].prioridad){
			temporal = task[j].prioridad; 
			temporal2= task[j].id;
			temporal3=task[j].tiempo_ejec;
			strcpy(temporal4,task[j].nom_task);
			task[j].prioridad = task[j+1].prioridad; 
			task[j].id = task[j+1].id;
			task[j].tiempo_ejec=task[j+1].tiempo_ejec;
			strcpy(task[j].nom_task,task[j+1].nom_task);
			task[j+1].prioridad = temporal;
			task[j+1].id=temporal2;
			task[j+1].tiempo_ejec=temporal3;
			strcpy(task[j+1].nom_task,temporal4);
			}
		}
	}
}
